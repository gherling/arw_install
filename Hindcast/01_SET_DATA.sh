#!/bin/bash

# start and end dates 
# of modeling (WRF long term)
a=2023
m=10
d0=01
df=31
# load environment variables to run WRF 
source ../../set_environment.sh

# Path of the WPS_GEOG directory with the data
# topography statics for geogrid.exe  
geo_data='/ARW/WPS_GEOG'
geo_data_path=${WHOME}${geo_data}
# Local directory of the domain
DHOME=$(pwd)

cp namelist.wps namelist.wps.back

sed -i "s|geog_data_path\s*=.*|geog_data_path = '${geo_data_path}',|" namelist.wps

# 'sed' command to replace the variable "opt_*.*_path = " by 
# a value stored in $DHOME in single quotes 
sed -i "s|opt_output_from_geogrid_path\s*=.*|opt_output_from_geogrid_path = '$DHOME',|" namelist.wps
sed -i "s|opt_output_from_metgrid_path\s*=.*|opt_output_from_metgrid_path = '$DHOME',|" namelist.wps
sed -i "s|opt_geogrid_tbl_path\s*=.*|opt_geogrid_tbl_path = '$DHOME',|" namelist.wps
sed -i "s|opt_metgrid_tbl_path\s*=.*|opt_metgrid_tbl_path = '$DHOME',|" namelist.wps

# 'sed' command to replace start and end values 
# for three nested domains within the namelist.wps file
sed -i "s|start_date\s*=.*|start_date = '${a}-${m}-${d0}_00:00:00',\
'${a}-${m}-${d0}_00:00:00',\
'${a}-${m}-${d0}_00:00:00',|" namelist.wps

sed -i "s|end_date\s*=.*|end_date   = '${a}-${m}-${df}_00:00:00',\
'${a}-${m}-${df}_00:00:00',\
'${a}-${m}-${df}_00:00:00',|" namelist.wps

cp namelist.input namelist.input.back

# Calculate the days to be modeled
run_days=$((df - d0))

# 'sed' command to replace a value of a variable within the 
#	namelist.input file using a bash environment variable
sed -i "s/run_days\s*=\s* .*/run_days         = ${run_days},/"  namelist.input
sed -i "s/start_year\s*=\s* .*/start_year       = ${a},${a},${a},/" namelist.input
sed -i "s/start_month\s*=\s* .*/start_month      = ${m},${m},${m},/" namelist.input
sed -i "s/start_day\s*=\s* .*/start_day        = ${d0},${d0},${d0},/"   namelist.input
sed -i "s/end_year\s*=\s* .*/end_year        = ${a},${a},${a},/"    namelist.input
sed -i "s/end_month\s*=\s* .*/end_month        = ${m},${m},${m},/"  namelist.input
sed -i "s/end_day\s*=\s* .*/end_day          = ${df},${df},${df},/"    namelist.input
