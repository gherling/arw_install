#!/bin/bash

## Remove all symbolic link matches
find -type l -exec rm {} +

rm *.nc rsl* FILE* *.log* wrfrst* wrfi* wrfb*  Vtable *.TBL *.parms

outdir='../../Outputs/'

if [ -d "$outdir" ]; then
  echo "The directory already exists."
else
  echo "The directory does not exist. Creating the directory...."
  mkdir $outdir
fi

mv wrfout_d0* $outdir

