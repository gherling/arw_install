# DESCRIPCION : DESCARGA DE CONDICIONES DE BORDE ECMWF "SINGLE LEVELS"

# CREACION    : ABRIL, 2021. https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form
#		En base a variables sugeridas por Xin Zhang (NUIST).	
	
# MODIFICACION: ABRIL, 2021. Katherine Munoz (kmunoz@geacore.com) 

# LANZAMIENTO : python get_ERA5_single_levels.py
#		Recuerde activar las API
 		
import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type':'reanalysis',
        'variable':[
            '10m_u_component_of_wind',
	    '10m_v_component_of_wind',
            '2m_temperature',
	    '2m_dewpoint_temperature',
	    'land_sea_mask',
	    'surface_pressure',
	    'mean_sea_level_pressure',
            'skin_temperature',
	    'sea_ice_cover',
	    'sea_surface_temperature',
            'snow_depth',
            'soil_temperature_level_1',
	    'soil_temperature_level_2',
            'soil_temperature_level_3',
	    'soil_temperature_level_4',
            'volumetric_soil_water_layer_1',
	    'volumetric_soil_water_layer_2',
            'volumetric_soil_water_layer_3',
            'volumetric_soil_water_layer_4',],
        'year': '2023',
        'month': '10',
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
        'time': ['00:00', '06:00', '12:00', '18:00',],
        'area': [
            45, -115, 
           -20, -28,
        ],
        'format':'grib',
    },
'ERA5_sl_20231031.grib')
