#!/bin/bash

# load environment variables to run WRF 
source ../../set_environment.sh

ln -s ../../WPS/* .
ln -s ../../WPS/geogrid/* .
ln -s ../../WPS/metgrid/* .
#ln -s ../../WRF/run/* .

# ERA5 data setup for initial and boundary conditions
ln -sf $WHOME/ARW/WPS/ungrib/Variable_Tables/Vtable.ECMWF Vtable

mpirun -np 1 ./geogrid.exe

echo " "
read -p " Finish GEOGRID, press Enter to continue..."

ln -s ./ERA5/ERA5_sl_20231031.grib 
./link_grib.csh ERA5_sl_20231031.grib

# 'sed' command to replace a value of a variable within the 
# namelist.wps file using a bash environment variable
sprefix="FILE_S"
sed -i "s/prefix\s*=\s* .*/prefix   = '${sprefix}',/"  namelist.wps

./ungrib.exe 

echo " "
read -p "Finish UNGRID:ERA5_sl, Press Enter to continue..."

rm ERA5_sl_*.grid
rm GRIBFILE.*

ln -s ./ERA5/ERA5_pl_20231031.grib
./link_grib.csh ERA5_pl_20231031.grib
sprefix="FILE_P"
sed -i "s/prefix\s*=\s* .*/prefix   = '${sprefix}',/"  namelist.wps

./ungrib.exe 

echo " "
read -p " Finish UNGRID:ERA5_pl, Press Enter to continue..."

# Sea Surface Temperature (SST) SST data configuration 
# for initial and boundary conditions
rm ERA5_pl_*.grid
rm GRIBFILE.*

ln -sf $WHOME/ARW/WPS/ungrib/Variable_Tables/Vtable.SST Vtable
ln -s ./SST/SST.2023.grb
./link_grib.csh SST.2023.grb

sprefix="SST"
sed -i "s/prefix\s*=\s* .*/prefix   = '${sprefix}',/"  namelist.wps

./ungrib.exe

echo " "
read -p " Finish UNGRID: SST, Press Enter to continue..."

sprefix="'FILE_P','FILE_S','SST' "
sed -i "s/fg_name\s*=\s* .*/fg_name  = ${sprefix}/"  namelist.wps

mpirun -np 4 ./metgrid.exe
echo " "
read -p " Finish METGRID: Press Enter to continue..."

