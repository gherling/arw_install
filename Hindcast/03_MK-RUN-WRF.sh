#!/bin/bash

# load environment variables to run WRF 
source ../../set_environment.sh
ln -s ../../WRF/run/* .

#export OMP_NUM_THREADS=40
#export MP_STACK_SIZE=64000000

mpirun -np 3 ./real.exe

echo " "
read -p "Press Enter to continue..."

echo "-------------------------"
echo " !!! Start Modelling !!!"
echo "-------------------------"

start_time=$(date +%s)

mpirun -np 70 ./wrf.exe

end_time=$(date +%s)
elapsed_time=$(echo "$end_time - $start_time" | bc)

echo "-------------------------"
echo " !!! Finish in $elapsed_time seg !!!"
echo "-------------------------"


#tail -f rsl.error.0000
