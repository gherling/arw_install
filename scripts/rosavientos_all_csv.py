#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

dataDir = '/home/magnus/Projects/2024/WRF_post/'
plotDir = dataDir + 'fig/'

#data_file01 = dataDir + 'csv/' + 'wrf_wind_d01_2023-01-01.csv'
data_file02 = dataDir + 'csv/' + 'wrf_wind_d02_2023-12-01.csv'
#data_file03 = dataDir + 'csv/' + 'wrf_wind_d03_2023-01-01.csv'


data = pd.read_csv(data_file02)

opcion=['wSpeed.200', 'wDir.200',
        'wSpeed.50' , 'wDir.50',
        'wSpeed.10' , 'wDir.10',]


# Crear el gráfico de rosa de los vientos
plt.figure(figsize=(5, 5))

ax = plt.subplot(polar=True)

#ax.hist(data['wDir.10']*np.pi/180., 
#         weights=None, 
#         edgecolor='red', color='skyblue')

ax.hist(data['wDir.10']*np.pi/180., 
#        weights=data['wSpeed.10']/175., 
         weights=None,
         edgecolor='red', color='skyblue')

plt.title('Diciembre 2023 (10m)', va='bottom')
# Añadir las etiquetas de las direcciones
ax.set_xticks(np.radians(np.arange(0, 360, 45)))
ax.set_xticklabels(['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'])


# Guardar el gráfico como un archivo pdf
plt.savefig( plotDir+'rosa-d2_12_s10.pdf', format='pdf' )
plt.show()
