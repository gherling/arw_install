#!/usr/bin/env python3
import pandas as pd
import matplotlib.pyplot as plt

dataDir = '/home/magnus/Projects/2024/WRF_post/csv/'
plotDir = dataDir + 'fig/'

data_file01 = dataDir + 'wrf_wind_d01_2023-11-01.csv'
data_file02 = dataDir + 'wrf_wind_d02_2023-11-01.csv'
data_file03 = dataDir + 'wrf_wind_d03_2023-11-01.csv'


data = pd.read_csv(data_file02)

opcion=['wSpeed.200',    
        'wSpeed.50' ,
        'wSpeed.10']

# Crear el gráfico de dispersión
ax = data.plot( x='time',  
                y=opcion,
                rot=30, 
                title='Noviembre 2023', xlabel='',
                ylabel='Velocity [m/s]', fontsize=9,
                figsize=(12, 4), kind='line' )

# Guardar el gráfico como un archivo pdf
plt.savefig( plotDir+'grafico.pdf', format='pdf', 
             bbox_inches = 'tight', pad_inches = 1 )

#data.columns
