#!/bin/bash
#
# Get SST files average daily for one year
# 0.25 degree latitude x 0.25 degree longitude global grid (1440x720)
#
# 12/09/2023 Efrain Rodriguez Rubio - Instituto Colombiano del Petroleo, Ecopetrol-SA.
# efrain.rodriguezru@ecopetrol.com.co
# 27/11/2023 version modified to bash by Herling Gonzalez A. (ICP-ECP)
# herling.gonzalez@ecopetrol.com.co

Y=2023

# Climate Data Operator (CDO), for standard processing of climate 
# and NWP model output (for versions higher or equal to 1.9.9).
# wget https://code.mpimet.mpg.de/attachments/download/23323/cdo-1.9.9.tar.gz
# tar xvzf cdo-1.9.9.tar.gz
# cd cdo-1.9.9
# ./configure --prefix=`pwd` --with-netcdf=${WHOME}/ARW/Library
# make && make install

wget -c https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2.highres/sst.day.mean.${Y}.nc

echo " "
read -p "Press Enter to continue... "
echo "....running netcdf conversion to grib format"

cdo selname,sst sst.day.mean.${Y}.nc file1.nc # Select parameters by name (SST)
cdo -f grb copy file1.nc file2.grb            # Copies all input datasets to ofile
cdo setltype,1 file2.grb file3.grb            # Set field info, for GRIB level type 1
cdo chcode,1,11 file3.grb file4.grb           # Change code number
cdo addc,273.15 file4.grb SST.${Y}.grb        # Add a constant 273.15 (Celsius to Kelvin)

#www.idris.fr/media/ada/cdo.pdf
