#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt

dataDir  = '/home/magnus/Projects/2024/WRF_post/'
eolosDir = '/home/magnus/Projects/2024/WRF_post/csv/'
plotDir = dataDir + 'fig/'

eolosData = 'eolos_out.csv'

dat = pd.read_csv( eolosDir+eolosData, 
                     delimiter=None, 
                     low_memory=False )
pd.options.display.max_columns = None

opcion=['LIDAR12m_WdSpdHor_Avg',        
        'LIDAR60m_WdSpdHor_Avg',
        'LIDAR200m_WdDir_Avg']

# 01-10-2023 00:00  ---  31-10-2023 23:00
data = dat.iloc[72:815:]

# Crear el gráfico de dispersión
ax = data.plot( x='timestamp',  
                y=opcion[0],
                rot=30, 
                title='Eolos Octubre 2023', xlabel='',
                ylabel='Velocity [m/s]', fontsize=9,
                figsize=(12, 4), kind='line' )

# Guardar el gráfico como un archivo pdf
plt.savefig( plotDir+'eolos_grafico.pdf', format='pdf', 
             bbox_inches = 'tight', pad_inches = 1 )