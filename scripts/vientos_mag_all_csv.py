#!/usr/bin/env python3
import pandas as pd
import matplotlib.pyplot as plt

dataDir = '/home/magnus/Projects/2024/WRF_post/'
plotDir = dataDir + 'fig/'

data_file01 = dataDir + 'csv/' + 'wrf_wind_d01_2023-10-01.csv'
data_file02 = dataDir + 'csv/' + 'wrf_wind_d02_2023-10-01.csv'
data_file03 = dataDir + 'csv/' + 'wrf_wind_d03_2023-10-01.csv'


data = pd.read_csv(data_file02)

opcion=['wSpeed.200',
        'wSpeed.50' ,
        'wSpeed.10']

# Crear el gráfico de dispersión
ax = data.plot( x='time',  
                y=opcion,
                rot=30, 
                title='Diciembre 2023 (d2)', xlabel='',
                ylabel='Velocity [m/s]', fontsize=9,
                figsize=(12, 4), kind='line' )

# Guardar el gráfico como un archivo pdf
plt.savefig( plotDir+'ts-worfout_d2_12.pdf', format='pdf', 
             bbox_inches = 'tight' )

#data.columns
