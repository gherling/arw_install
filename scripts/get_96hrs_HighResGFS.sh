#!/bin/bash
#
# Get GFS files for the next 96h, every 6h, 0.25°
#
# 28/09/2012 Andres Sepulveda - University of Concepcion (andres@dgeo.udec.cl)
# Adapted from a script of Rodrigo Sanchez - University of Chile (DGF)
#
# 10/04/2015 Updated gfs file name - Andres Sepulveda (DGEO)
# 20/06/2019 Updated gfs path      - Andres Sepulveda (DGEO)
# 27/08/2023 version modified to bash by Herling Gonzalez A. (ECP)
# herling.gonzalez@ecopetrol.com.co

f=`date +%Y%m%d`
URL="ftp://ftpprd.ncep.noaa.gov/pub/data/nccf/com/gfs/prod"
mkdir $f
cd $f

for hracb in 00; do
  for hra in 00 06 12 18 24 30 36 42 48 54 60 66 72 78 84 90 96; do
    wget -t 0 "${URL}/gfs.${f}/${hracb}/atmos/gfs.t${hracb}z.pgrb2.0p25.f0${hra}"
  done
done
