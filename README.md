# ARW_install

## Getting started

The WRF installation scripts will install WRF and WPS along with the necessary libraries (netcdf4, hdf5, mpich, zlib, libpng, jasper). These scripts are written for Debian and Ubuntu-based Linux operating systems, such as Ubuntu, Debian, Linux Mint, etc. NOTE: This scripts assumes you are using the BASH shell.

```
export WHOME=`cd;pwd`
export DIR=${WHOME}/ARW/Library
export CC=gcc
export CXX=g++
export F77=gfortran
export FC=gfortran
export FFLAGS=-m64
export FCFLAGS=-m64
export CPPFLAGS=-I$DIR/include 
export LDFLAGS=-L$DIR/lib
export HDF5=$DIR
export NETCDF=$DIR
export LIBS="-lnetcdf -lhdf5_hl -lhdf5 -lz" 
export JASPERLIB=$DIR/lib
export JASPERINC=$DIR/include
export LD_LIBRARY_PATH=$DIR/lib:$LD_LIBRARY_PATH
export PATH=$DIR/bin:$PATH
```

## Name
WRF installation scripts.

## Description

The Advanced Research WRF (WRF-ARW) model has been developed over several years. The WRF model is a flexible, portable, and computationally efficient atmospheric simulation system for parallel computing platforms. It is suitable for use at various scales, from meters to thousands of kilometers, for a wide range of applications, including the following:

1. Research Applications
2. Idealized Simulations
3. Data Assimilation
4. Real-Time Numerical Weather Prediction
5. Forecasting
6. Model Coupling
7. Tropical Cyclones
8. Regional Climate
9. Wildfires
10. Atmospheric Chemistry
11. Hydrogeology

## Installation

```
git clone https://gitlab.com/gherling/arw_install
cd arw_install
bash ARW_install_4.X.sh
source $WHOME/ARW/set_environment.sh
mkdir $WHOME/ARW/Domains
cp -r Forecast/ $WHOME/ARW/Domains/
cp -r Hindcast/ $WHOME/ARW/Domains/
```

```
Note: the custom directory $WHOME prefix is where the WRF installation is located
```


## Forecast Usage

```
cd $WHOME/ARW/Domains/Forecast
bash 01_SET_DATA_TIME.sh
bash 02_MK-BNDRY.sh 
bash 03_MK-RUN-WRF.sh
```

## Hindcast Usage

```
cd $WHOME/ARW/Domains/Hindcast
cd ERA5/
python get_ERA5_pressure_levels.py
python get_ERA5_single_levels.py
cd ../SST/
bash get_HighResSST.sh
cd ..
bash 01_SET_DATA.sh
bash 02_MK-BNDRY.sh 
bash 03_MK-RUN-WRF.sh
```


## Add your files
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gherling/arw_install.git
git branch -M main
git push -uf origin main
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This work is licensed under the GNU General Public License (GPL) v3.0. See the
LICENSE file for more details.
