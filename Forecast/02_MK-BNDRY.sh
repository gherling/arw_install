#!/bin/bash

a=`date +%Y`
m=`date +%m`
d=`date +%d`

source ../../set_environment.sh

ln -s ../../WPS/* .
ln -s ../../WPS/geogrid/* .
ln -s ../../WPS/metgrid/* .
ln -s ../../WRF/run/* .

mpirun -np 1 ./geogrid.exe

echo " "
read -p "Press Enter to continue ..."

#./link_grib.csh ../../DATA_GFS/${a}${m}${d}/gfs.t00z.pgrb2.0p50.f0*
./link_grib.csh ../../DATA_GFS/${a}${m}${d}/gfs*

cp ../../WPS/ungrib/Variable_Tables/Vtable.GFS Vtable

./ungrib.exe 

echo " "
read -p "Press Enter to continue ..."

mpirun -np 4 ./metgrid.exe

