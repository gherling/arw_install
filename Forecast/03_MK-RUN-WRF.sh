#!/bin/bash

source ../../set_environment.sh

#export OMP_NUM_THREADS=40
#export MP_STACK_SIZE=64000000

mpirun -np 1 ./real.exe

echo " "
read -p "Press Enter to continue..."

echo "-------------------------"
echo " !!! Start Modeling !!!"
echo "-------------------------"

start_time=$(date +%s)

mpirun -np 8 ./wrf.exe

end_time=$(date +%s)
elapsed_time=$(echo "$end_time - $start_time" | bc)

echo "-------------------------"
echo " !!! Finish in $elapsed_time seg !!!"
echo "-------------------------"


#tail -f rsl.error.0000
