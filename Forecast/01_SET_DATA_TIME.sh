#!/bin/bash
# Obtener archivos GFS para las próximas 96h, cada 6h, 0.50°

a=`date +%Y`
m=`date +%m`
d=`date +%d`
m2=`date -d "+4 days" +%m`  #estima el mes en los proximos 4 dias
d2=`date -d "+4 days" +%d`  #estima el dia en los proximos 4 dias 

dirb="../../DATA_GFS/" #Directorio base para archivos GFS

mkdir $dirb

dirf=$dirb$a$m$d #nombre del directorio con fecha de hoy

URL="ftp://ftpprd.ncep.noaa.gov/pub/data/nccf/com/gfs/prod"

# Directorio local del dominio
DHOME=$(pwd)

if [ -d "$dirf" ]; then
  echo "El directorio ya existe."
  cd ${DHOME}
else
  echo "El directorio no existe. Creando el directorio..."
  mkdir $dirf 
  echo "Directorio creado."
  cd $dirf
  for hracb in 00; do
    for hra in 00 06 12 18 24 30 36 42 48 54 60 66 72 78 84 90 96; do
      wget -t 0 "${URL}/gfs.${a}${m}${d}/${hracb}/atmos/gfs.t${hracb}z.pgrb2.0p25.f0${hra}"
    done
  done
  cd ${DHOME}
fi

run_hours=96

cp namelist.input namelist.input.back

# command 'sed' to replace the value of a variable within the 
# namelist.input  file using a bash environment variable
sed -i "s/start_year\s*=\s* .*/start_year  = ${a},${a},/"  namelist.input
sed -i "s/end_year\s*=\s* .*/end_year    = ${a},${a},/"    namelist.input
sed -i "s/run_hours\s*=\s* .*/run_hours   = ${run_hours},/"  namelist.input
sed -i "s/start_month\s*=\s* .*/start_month = ${m},  ${m},/" namelist.input
sed -i "s/start_day\s*=\s* .*/start_day   = ${d},  ${d},/"   namelist.input
sed -i "s/end_month\s*=\s* .*/end_month   = ${m2}, ${m2},/"  namelist.input
sed -i "s/end_day\s*=\s* .*/end_day     = ${d2}, ${d2},/"    namelist.input

echo " "
echo "Se ha modificado namelist.input "
read -p "Presiona Enter para continuar..."

cp namelist.wps namelist.wps.back

# comando 'sed' para reemplazar un valor de una 
# variable dentro del archivo namelist.wps
sed -i "s|start_date\s*=.*|start_date = '${a}-${m}-${d}_00:00:00', '${a}-${m}-${d}_00:00:00',|"   namelist.wps
sed -i "s|end_date\s*=.*|end_date   = '${a}-${m2}-${d2}_00:00:00', '${a}-${m2}-${d2}_00:00:00',|" namelist.wps

# comando 'sed' para reemplazar la variable "opt_*.*_path = " por 
# un valor guardado en $DHOME en comillas sencillas 
sed -i "s|opt_output_from_geogrid_path\s*=.*|opt_output_from_geogrid_path = '$DHOME',|" namelist.wps
sed -i "s|opt_output_from_metgrid_path\s*=.*|opt_output_from_metgrid_path = '$DHOME',|" namelist.wps
sed -i "s|opt_geogrid_tbl_path\s*=.*|opt_geogrid_tbl_path = '$DHOME',|" namelist.wps
sed -i "s|opt_metgrid_tbl_path\s*=.*|opt_metgrid_tbl_path = '$DHOME',|" namelist.wps

# Directorio ARW-WRF con la ruta de los datos de entrada estaticos de WPS descargados
source ../../set_environment.sh
geo_data='/ARW/WPS_GEOG'
geo_data_path=${WHOME}${geo_data}

sed -i "s|geog_data_path\s*=.*|geog_data_path = '${geo_data_path}',|" namelist.wps


echo " "
echo "Se ha modificado namelist.wps "
read -p "Presiona Enter para continuar..."
