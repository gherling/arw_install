#!/bin/bash

# Download and install required library and data files for Advanced Research WRF (ARW).
# License: LGPL
# Herling Gonzalez Alvarez <herling.gonzalez@ecopetrol.com.co>
# Tested in Ubuntu 18.04 LTS
# Tested in Ubuntu 20.04 LTS
# Update: Efrain Rodriguez Rubio <efrain.rodriguezru@ecopetrol.com.co>
# Tested in Ubuntu 22.04 LTS

# basic package managment
# sudo apt update
# sudo apt upgrade
# sudo apt install gcc gfortran g++ libtool automake autoconf make m4 grads default-jre csh

####################################################
#### Directory Listing
export WHOME=`cd;pwd`
mkdir $WHOME/ARW
cd $WHOME/ARW
mkdir Downloads
mkdir Library
export DIR=${WHOME}/ARW/Library

####################################################
#### Compilers and flags
export CC=gcc
export CXX=g++
export F77=gfortran
export FC=gfortran
export FFLAGS=-m64
export FCFLAGS=-m64

####################################################
#### Downloading Libraries
cd Downloads
wget -c https://www.zlib.net/zlib-1.3.1.tar.gz
wget -c https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.5/src/hdf5-1.10.5.tar.gz
wget -c https://downloads.unidata.ucar.edu/netcdf-c/4.9.0/netcdf-c-4.9.0.tar.gz
wget -c https://downloads.unidata.ucar.edu/netcdf-fortran/4.6.0/netcdf-fortran-4.6.0.tar.gz
wget -c http://www.mpich.org/static/downloads/3.3.1/mpich-3.3.1.tar.gz
wget -c https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz
wget -c https://www.ece.uvic.ca/~frodo/jasper/software/jasper-1.900.1.zip
wget -c https://code.mpimet.mpg.de/attachments/download/23323/cdo-1.9.9.tar.gz

####################################################
#### zlib
cd $WHOME/ARW/Downloads
tar -xvzf zlib-1.3.1.tar.gz
cd zlib-1.3.1/
./configure --prefix=$DIR
make
make install


####################################################
#### hdf5 library for netcdf4 functionality
cd $WHOME/ARW/Downloads
tar -xvzf hdf5-1.10.5.tar.gz
cd hdf5-1.10.5
./configure --prefix=$DIR --with-zlib=$DIR --enable-hl --enable-fortran
make check
make install
export HDF5=$DIR
export LD_LIBRARY_PATH=$DIR/lib:$LD_LIBRARY_PATH

####################################################
#### Install NETCDF C Library
cd $WHOME/ARW/Downloads
tar -xvzf netcdf-c-4.9.0.tar.gz
cd netcdf-c-4.9.0/
export CPPFLAGS=-I$DIR/include 
export LDFLAGS=-L$DIR/lib
./configure --prefix=$DIR --disable-dap
make check
make install
export PATH=$DIR/bin:$PATH
export NETCDF=$DIR

####################################################
#### NetCDF fortran library
cd $WHOME/ARW/Downloads
tar -xvzf netcdf-fortran-4.6.0.tar.gz
cd netcdf-fortran-4.6.0/
export LD_LIBRARY_PATH=$DIR/lib:$LD_LIBRARY_PATH
export CPPFLAGS=-I$DIR/include 
export LDFLAGS=-L$DIR/lib
export LIBS="-lnetcdf -lhdf5_hl -lhdf5 -lz" 
./configure --prefix=$DIR --disable-shared
make check
make install

#####################################################
#### MPICH
cd $WHOME/ARW/Downloads
tar -xvzf mpich-3.3.1.tar.gz
cd mpich-3.3.1/
./configure --prefix=$DIR
make
make install
export PATH=$DIR/bin:$PATH

#####################################################
#### OPENMPI
##cd $WHOME/ARW/Downloads
##tar -xvzf openmpi-5.0.1.tar.gz
##cd openmpi-5.0.1/
##./configure --prefix=$DIR
##make
##make install
##export PATH=$DIR/bin:$PATH

#####################################################
### libpng
cd $WHOME/ARW/Downloads
export LDFLAGS=-L$DIR/lib
export CPPFLAGS=-I$DIR/include
tar -xvzf libpng-1.2.50.tar.gz
cd libpng-1.2.50/
./configure --prefix=$DIR
make
make install

#####################################################
### JasPer
cd $WHOME/ARW/Downloads
unzip jasper-1.900.1.zip
cd jasper-1.900.1/
autoreconf -i
./configure --prefix=$DIR
make
make install
export JASPERLIB=$DIR/lib
export JASPERINC=$DIR/include

#####################################################
### Climate Data Operator (CDO)
cd $WHOME/ARW/Downloads
tar -xvzf cdo-1.9.9.tar.gz 
cd cdo-1.9.9
./configure --prefix=$DIR --with-netcdf=$DIR
make
make install


######################################################################
############################ WRF 4.5 #################################
cd $WHOME/ARW
git clone --recurse-submodules https://github.com/wrf-model/WRF
#wget -c https://github.com/wrf-model/WRF/archive/v4.3.3.tar.gz
#tar -xvzf v4.3.3.tar.gz -C $WHOME/ARW --transform 's/^WRF-4.3.3/WRF/'
cd $WHOME/ARW/WRF
./clean
(echo 34; echo 1) | ./configure # 34, 1 for gfortran and distributed memory
./compile em_real > compiling.log
export WRF_DIR=$WHOME/ARW/WRF


######################################################################
############################ WPS 4.5 #################################
cd $WHOME/ARW
git clone https://github.com/wrf-model/WPS
#wget -c https://github.com/wrf-model/WPS/archive/v4.3.tar.gz
#tar -xvzf v4.3.tar.gz -C $WHOME/ARW --transform 's/^WPS-4.3/WPS/'
cd $WHOME/ARW/WPS
./clean
echo 3 | ./configure 
cp configure configure.back
sed -i -e 's/-lnetcdff -lnetcdf/-lnetcdff -lnetcdf -lz -lgomp -lhdf5/g' $WHOME/ARW/WPS/configure.wps
./compile > compiling.log


######################################################################
################## ARWpost: Post-Processing Tool #####################
### ARWpost
cd $WHOME/ARW/Downloads
wget -c http://www2.mmm.ucar.edu/wrf/src/ARWpost_V3.tar.gz
tar -xvzf ARWpost_V3.tar.gz -C $WHOME/ARW
cd $WHOME/ARW/ARWpost
./clean -a
sed -i -e 's/-lnetcdf/-lnetcdff -lnetcdf/g' $WHOME/ARW/ARWpost/src/Makefile
echo 3 | ./configure #3 gfortran compiler
sed -i -e 's/-C -P/-P/g' $WHOME/ARW/ARWpost/configure.arwp
#sed -i -e 's/-frecord-marker=4/-frecord-marker=4 -fallow-argument-mismatch/g' $WHOME/ARW/ARWpost/configure.arwp
./compile

######################################################################
######################## Static Geography Data #######################
cd $WHOME/ARW/Downloads
wget -c https://www2.mmm.ucar.edu/wrf/src/wps_files/geog_high_res_mandatory.tar.gz
tar -xvzf geog_high_res_mandatory.tar.gz -C $WHOME/ARW


######################################################################
######################## Set Bash Environment ########################
echo "" > ${WHOME}/ARW/set_environment.sh
set_file=("### ARW-WRF Variables ###" 
          "export WHOME=${WHOME}" 
          "export DIR="\$"{WHOME}/ARW/Library" 
          "export CC=gcc" 
          "export CXX=g++" 
          "export FC=gfortran" 
          "export FCFLAGS=-m64" 
          "export F77=gfortran" 
          "export FFLAGS=-m64" 
          "export CPPFLAGS=-I"\$"{DIR}/include" 
          "export LDFLAGS=-L"\$"{DIR}/lib" 
          "export NETCDF="\$"{DIR}" 
          "export HDF5="\$"{DIR}" 
          "export LIBS="\""-lnetcdf -lhdf5_hl -lhdf5 -lz"\""" 
          "export JASPERLIB="\$"{DIR}/lib" 
          "export JASPERINC="\$"{DIR}/include" 
          "export LD_LIBRARY_PATH="\$"{DIR}/lib:"\$"{LD_LIBRARY_PATH}" 
          "export PATH="\$"{DIR}/bin:"\$"{PATH}")

for row in "${set_file[@]}" ; do
echo "${row}" >> ${WHOME}/ARW/set_environment.sh
done

